
Solace Search
=============

This module is a proof of concept, and an simple and nice example to show how to
use the Solace API module.
This module may evolve to a better module, but don't take it seriously and avoid
to use it on production site. If you are a developer which intend to use Solace
API, this module is for you, else, you may want to test it, but that's it.

This module allows you to use Solr for global site search. It does provide the
same feature that the 'apachesolr_search' module. The main difference is how is
built and handled the Solr query.

This module allows you to build a complex query using a nice and user friendly
AJAX form.

Installation
============

Just install and enable the 'Solace API' module and this one. You can then
directly go to the 'search/assistant' path to test it.

Status and caveats
==================

If 'apachesolr_search' module is enabled, and configured to be the default
search engine, Solace Search won't be the default search engine. Else, it
will (per default, and always) replace the drupal default search engine.

If the 'search' module is disabled, the only way you can access the feature
if through the 'search/assistant' path. May be a block will be implemented
in the future in order to get rid of the infamous core 'search' module.

Future plans
============

 - We intend to use 'apachesolr' module to do faceted search. We also have to
   reimplement the paged results.
 - Ability for users to save and manage their custom filters would be a great
   thing.
 - CCK integration could be something great to.

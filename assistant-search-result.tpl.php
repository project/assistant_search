<?php
?>
<div class="assistant-search-result">
  <?php if ($score): print $score; endif; ?>
  <h4><?php print $title; ?></h4>
  <div class="result-content">
    <?php if ($teaser): ?>
      <div class="teaser">
        <?php print $teaser; ?>
      </div>
    <?php endif; ?>
    <div class="author"><?php print $author_link; ?></div>
    <?php if ($changed_string): ?>
      <div class="update"><?php print $changed_string; ?></div>
    <?php endif; ?>
    <?php if (isset($content)): ?>
      <div class="content"><?php print $content; ?></div>
    <?php endif; ?>
    <?php if ($comment_count > 0): ?>
      <div class="comments"><?php print $comment_string; ?></div>
    <?php endif; ?>
  </div>
</div>

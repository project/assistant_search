<?php
?>
<div class="assistant-search-result-page">
  <div class="search-results">
    <?php if ($message && $pager): ?>
      <p style="text-align: center;"><?php print $message; ?></p>
    <?php endif; ?>
    <?php foreach ($results as $result): ?>
      <div class="search-result">
        <?php print $result; ?>
      </div>
    <?php endforeach; ?>
  </div>
  <div class="search-message">
    <?php if ($pager): ?>
      <p><?php print $pager; ?></p>
    <?php endif; ?>
    <?php if ($message): ?>
      <p style="text-align: center;"><?php print $message; ?></p>
    <?php endif; ?>
  </div>
</div>
